#!/bin/bash
docker_id="kairosaki"
docker_token="a0f79306-57df-4c3e-9ee8-21395d4779f5"

#### Delete services
echo "Deleting services ..."
ssh db1 docker service rm debianswarm_webappcluster

#### Delete remaining volumes on workers
echo "sleeeping for 2 seconds ..."
sleep 2
echo "Deleting remaining volumes on workers ..."

#### Docker login on all nodes
echo "Trying docker login on all nodes ..."
for i in {1..4}
do
  ssh -o StrictHostKeyChecking=no db$i hostname
  echo "Enter password : "
  ssh -o StrictHostKeyChecking=no db$i docker login -u $docker_id -p $docker_token
done

#### Docker image pull on workers
echo "Trying to pull images ... stay calm !!!"
for i in {1..4}
do
  ssh db$i docker image pull $docker_id/flasky:latest
done

#### Launch services, volumes and networks
echo "sleeeping for 10 seconds ..."
sleep 3
echo "Launch services, volumes and networks ..."
ssh db1 docker stack deploy -c /srv/docker-compose.yml debianswarm
