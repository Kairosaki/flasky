FROM debian:bullseye-20220418

RUN apt-get update && \
    apt-get install -y python3 && \ 
    apt-get install -y python3-pip && \
    apt-get install -y htop

COPY . /srv

WORKDIR /srv

RUN pip3 install -r requirements.txt

# SOLUTION 1 gunicorn
CMD [ "gunicorn", "--bind=0.0.0.0:9090", "hello:app" ]

# SOLUTION 2 flask

#ENV FLASK_APP=/srv/hello.py

#CMD [ "flask", "run", "--host=0.0.0.0", "--port=5050" ]
